import React from "react";

let inlinedStyles = "";
if (process.env.NODE_ENV === "production") {
  try {
    inlinedStyles = require("!raw-loader!../public/styles.css");
  } catch (e) {
    console.log(e);
  }
}

export default class HTML extends React.Component {
  render() {
    let css;
    if (process.env.NODE_ENV === "production") {
      css = (
        <style
          id="gatsby-inlined-css"
          dangerouslySetInnerHTML={{ __html: inlinedStyles }}
          />
      )
    }
    return (
      <html lang="en">
        <head>
          <meta charSet="utf-8" />
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1.0"
            />
          <link
            href='//fonts.googleapis.com/css?family=Merriweather:900,900italic,300,300italic'
            rel='stylesheet' type='text/css'
            />
          <link
            href='//fonts.googleapis.com/css?family=Lato:900,300'
            rel='stylesheet' type='text/css'
            />
          {this.props.headComponents}
          {css}
        </head>
        <body>
          <div
            id="___gatsby"
            dangerouslySetInnerHTML={{ __html: this.props.body }}
            />
          {this.props.postBodyComponents}
        </body>
      </html>
    );
  }
}

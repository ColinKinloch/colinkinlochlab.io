import React from "react";
import { Link } from "gatsby";
import { range } from 'lodash';


export default class Paginator extends React.Component {
  render() {
    const {index, count, path} = this.props
    const itp = (index, disable = false) => disable?undefined:`${path}${((index <= 1)?'':index)}`
    return (
      <div className="paginator">
        { (count > 1) ? (
          <span>
            { itp(index-1, index == 1) ? (
              <Link to={itp(index-1, index == 1)}>Previous</Link>
            ) : (
              <span>Previous</span>
            ) }
            <span>{ range(1, count+1).map(i =>
              <Link
                className={(i == index)?'current':''}
                to={itp(i, (i == index))}>{
                  i
              }</Link>
            )}</span>
            { itp(index+1, index == count) ? (
              <Link to={itp(index+1, index == count)}>Next</Link>
            ) : (
              <span>Next</span>
            ) }
          </span>
        ) : null}
      </div>
    );
  }
}

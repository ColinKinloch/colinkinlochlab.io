---
title: Tester
date: 2018-06-10
tags:
  - hello
  - world
category: Hello World
language: en
---

Here is an equation: 

$$
\lambda = \frac{a}{b}
$$

The variable $\lambda$ is lambda of the function.

Here it is $${ \lambda }$$ there it was.

<video autoplay="true" loop="true" id="yolo" class="fit">
  <source type="video/webm" src="PixelBounce.webm"/>
  <source type="video/mp4" src="PixelBounce.mp4"/>
</video>

[pixel_video]: PixelBounce.webm

``` rust
mod fibonacci;
use fibonacci::fibonacci;

fn main() {
  println!("{:?}", fibonacci(6));
}
```

Source line numbering and headers would be nice.

<aside class="right">
  It is important to note
</aside>

``` rust
static fib: &[u8] = &[1, 1, 2, 3, 5, 8];

pub fn fibonacci(count: usize) -> Vec<u8> {
  let mut out = vec![0; count];
  for i in 0..count {
    out[i] = fib[i];
  }
  out
}
```

Source line numbering and headers would be nice.

<figure class="right sm-col-6 md-col-4 p2 border">
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/Barracudasauroides_panxianensis_MHNT_Chine.jpg/800px-Barracudasauroides_panxianensis_MHNT_Chine.jpg" />
</figure>



Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer enim lacus, convallis id suscipit ut, semper nec velit. Phasellus sit amet dolor ex. Ut nec aliquet enim, nec viverra diam. Aenean pretium odio a scelerisque cursus. Donec malesuada interdum ligula eget fermentum. Proin commodo turpis eget accumsan sodales. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur in ipsum et lectus porta accumsan eu non nibh. Suspendisse potenti. Nulla at pretium lacus. Nunc eu lorem a justo consequat cursus.

Proin ut orci cursus tellus luctus lobortis. Vestibulum eget turpis eget libero vestibulum hendrerit et non mi. Pellentesque interdum ac turpis a fermentum. Duis sapien ipsum, luctus vitae semper nec, consequat vel turpis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vestibulum volutpat mattis nunc, eu aliquet est gravida ut. Nulla dictum turpis id imperdiet vestibulum.

<audio class="fit" controls>
  <source type="audio/ogg" src="Chop-28-4.ogg"/>
  <source type="audio/flac" src="Chop-28-4.flac"/>
  <source type="audio/mpeg" src="Chop-28-4.mp3"/>
  <p>HTML5 audio is unsupported by your browser.</p>
</audio>

[test_audio_ogg]: Chop-28-4.ogg
[test_audio_flac]: Chop-28-4.flac
[test_audio_mpeg]: Chop-28-4.mp3

<!--<script gatsby-run src="hello.wasm"></script>-->

<script gatsby-run src="hello.js"></script>

<script gatsby-run src="simple.js"></script>

<script gatsby-run>
  setInterval(() => {console.log('uuu')}, 1000)
</script>

<iframe src="./random-floats-normal.html" onload="iFrameResize(this)">
</iframe>

[random-floats-normal]: random-floats-normal.html

~~hello~~

[simple_js]: simple.js

[test_script_wasm]: hello.wasm

[test_script_jsu]: hello.js

>残酷な天使のように  
>少年よ　神話になれ
>
>青い風がいま  
>胸のドアを叩いても  
>私だけをただ見つめて  
>微笑んでるあなた  
>そっとふれるもの  
>もとめることに夢中で  
>運命さえまだ知らない  
>いたいけな瞳  
>
>だけどいつか気付くでしょう　その背中には  
>遥か未来　めざすための　羽根があること  
>
>残酷な天使のテーゼ  
>窓辺からやがて飛び立つ  
>ほとばしる熱いパトスで  
>思い出を裏切るなら  
>この宇宙(そら)を抱いて輝く  
>少年よ　神話になれ
>
>ずっと眠ってる  
>私の愛の揺りかご  
>あなただけが　夢の使者に  
>呼ばれる朝がくる  
>細い首筋を  
>月あかりが映してる  
>世界中の時を止めて  
>閉じこめたいけど
>
>もしもふたり逢えたことに　意味があるなら  
>私はそう　自由を知る　ためのバイブル
>
>残酷な天使のテーゼ  
>悲しみがそしてはじまる  
>抱きしめた命のかたち  
>その夢に目覚めたとき  
>誰よりも光を放つ  
>少年よ　神話になれ
>
>人は愛をつむぎながら　歴史をつくる  
>女神なんてなれないまま　私は生きる
>
> <cite>Thomas Edison, AD 1995</cite>

Here is an image off of the wikipedia:

![An image off of the wikipedia](https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/Barracudasauroides_panxianensis_MHNT_Chine.jpg/800px-Barracudasauroides_panxianensis_MHNT_Chine.jpg)

const _ = require('lodash')

const rssPath = '/feed.xml'

const config = {
  siteMetadata: {
    title: '// TODO:',
    author: 'Colin Kinloch',
    email: 'colin@kinlo.ch',
    siteUrl: 'https://colin.kinlo.ch',
    description: `'ere I am.`,
    libravatar: require("crypto").createHash("sha256").update("colin@kinlo.ch").digest('hex'),
    language: 'en',
    postsPerPage: 7,
    rssMetadata: {
      site_url: 'colin.kinlo.ch',
      feed_url: `colin.kinlo.ch${rssPath}`,
      title: '// TODO:',
      description: 'desc',
      author: 'ck',
      copyright: 'ckr',
    },
    nav: [
      { url: '/posts/',
        title: '/posts/',
        tip: 'Valuable opinion', },
      { url: '/projects/',
        title: '/projects/',
        tip: 'Idle schemes', },
      { url: '/demos/',
        title: '/demos/',
        tip: 'Follies', },
      //{ url: '/black-whole/',
      //  title: 'noop',
      //  tip: 'Goes nowhere', },
    ],
    social: [
      { site: 'gitlab',
        name: 'ColinKinloch', },
      { site: 'github',
        name: 'ColinKinloch', },
      { site: 'twitter',
        name: 'ColinKinloch', },
      { site: 'mastodon',
        name: 'ColinKinloch', },
      { site: 'stack-overflow',
        name: 'ColinKinloch', },
      { site: 'linkedin',
        name: 'ColinKinloch', },
      { site: 'email',
        name: 'colin@kinlo.ch', },
      { site: 'rss', },
    ]
  },
  plugins: [
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'post',
        path: `${__dirname}/content/posts`,
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'page',
        path: `${__dirname}/content/pages`,
      },
    },
    /*{
      resolve: 'gatsby-source-medium',
      options: {
        username: 'colinkinloch',
      },
    },*/
    {
      resolve: 'gatsby-transformer-remark',
      options: {
        plugins: [
          'gatsby-remark-prismjs',
          'gatsby-remark-autolink-headers',
          'gatsby-remark-katex',
          'gatsby-remark-copy-ref',
        ],
      },
    },
    //'gatsby-remark-run-code',
    //'gatsby-plugin-postcss',
    'gatsby-plugin-sass',
    //'gatsby-plugin-sitemap',
    {
      resolve: 'gatsby-plugin-feed',
      options: {
        query: `
          {
            site {
              siteMetadata {
                title
                description
                siteUrl
                site_url: siteUrl
              }
            }
          }
        `,
        feeds: [
          {
            serialize: ({ query: { site, allMarkdownRemark } }) => {
              return allMarkdownRemark.edges.map(edge => {
                return Object.assign({}, edge.node.frontmatter, {
                  description: edge.node.excerpt
                });
              });
            },
            query: `
              {
                allMarkdownRemark(
                  limit: 1000,
                  sort: { order: DESC, fields: [frontmatter___date] }
                ) {
                  edges {
                    node {
                      excerpt
                      html
                      fields { slug }
                      frontmatter {
                        title
                        date
                      }
                    }
                  }
                }
              }
            `,
            output: rssPath,
          }
        ]
      }
    }
  ]
}

const social_defaults = {
  github: {
    url: 'https://github.com/%u',
    fa_set: 'fab',
    tip: 'GitHub',
  },
  gitlab: {
    url: 'https://gitlab.com/%u',
    fa_set: 'fab',
    tip: 'GitLab',
  },
  telegram: {
    url: 'tg:%u',
    fa_set: 'fab',
  },
  twitter: {
    url: 'https://twitter.com/%u',
    fa_set: 'fab',
    tip: 'Twitter',
  },
  mastodon: {
    url: 'https://mastodon.social/@%u',
    fa_set: 'fab',
    tip: 'Mastodon',
  },
  'stack-overflow': {
    url: 'https://stackoverflow.com/users/%u',
    fa_set: 'fab',
    tip: 'Stack Overflow',
  },
  linkedin: {
    url: 'https://www.linkedin.com/in/%u',
    fa_set: 'fab',
    tip: 'LinkedIn',
  },
  btc: {
    url: 'bitcoin:%u',
    tip: 'bitcoin'
  },
  email: {
    url: 'mailto:%u',
    fa_icon: 'envelope',
    tip: "e-mail",
  },
  rss: {
    url: rssPath,
    tip: "RSS",
  },
};

{
  const social = config.siteMetadata.social;
  for (const i in social) {
    const s = social[i]
    social[i] = Object.assign({}, social_defaults[s.site], s)
  }
}

module.exports = config

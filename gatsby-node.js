const print = console.log

const { GraphQLList } = require("graphql")

const path = require("path");
const createPaginatedPages = require('gatsby-paginate');
const _ = require("lodash");
const {
  kebabCase,
  padStart,
  upperFirst,
  lowerCase,
  get,
} = require("lodash");

const renderDrafts = process.env['gatsby_executing_command'] === 'develop'
const postsPerPage = require('./gatsby-config.js').siteMetadata.postsPerPage
console.log(postsPerPage)

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions;
  let slug;
  if (node.internal.type === "MarkdownRemark") {
    const fileNode = getNode(node.parent);
    const articleType = fileNode.sourceInstanceName
    const parsedFilePath = path.parse(fileNode.relativePath);
    // Set slug to specified slug
    if (_.has(node, "frontmatter.slug")) {
      slug = `/${kebabCase(node.frontmatter.slug)}/`;
    }
    // Set slug to posts/YYYY/MM/DD/title
    else if (
      _.has(node, "frontmatter.date") &&
      _.has(node, "frontmatter.title") &&
      articleType === 'post'
    ) {
      // Get "YYYY/MM/DD" or "NaN/NaN/NaN"
      // TODO: Correct for "Invalid date" / no date
      const d = new Date(node.frontmatter.date)
      let datePath = `${padStart(d.getFullYear(), 4, 0)}/`
      datePath += `${padStart(d.getMonth() + 1, 2, 0)}/`
      datePath += `${padStart(d.getDate(), 2, 0)}`
      slug = `/posts/${datePath}/${kebabCase(node.frontmatter.title)}/`;
    }
    // Set slug to file path/name
    else if (parsedFilePath.name !== "index" && parsedFilePath.dir !== "") {
      slug = `/${parsedFilePath.dir}/${parsedFilePath.name}/`;
    }
    // Set slug to file name
    else if (parsedFilePath.dir === "") {
      slug = `/${parsedFilePath.name}/`;
    }
    // Set slug to file path
    else {
      slug = `/${parsedFilePath.dir}/`;
    }
    createNodeField({ node, name: "slug", value: slug });
    createNodeField({ node, name: "articleType", value: articleType });
    createNodeField({ node, name: 'draft', value: node.frontmatter.draft || false });
    
    // Normalise tags
    const tags = get(node, 'frontmatter.tags');
    if (tags) {
      const fieldTags = tags.map(kebabCase)
      createNodeField({ node, name: 'tags', value: fieldTags});
    }
  }
}

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions
  
  //const posts = new Promise((resolve, reject) => {
  const createPosts = async () => {
    const postTemplate = path.resolve("./src/templates/post.jsx")
    const tagTemplate = path.resolve("./src/templates/tag.jsx")
    const postsTemplate = path.resolve("./src/templates/posts.jsx")
    
    const result = await graphql(`
      {
        allMarkdownRemark(
          filter: {
            fields: {
              articleType: { eq: "post" }
              ${ renderDrafts ? '' : 'draft: { ne: true }' }
            }
          }
          sort: { fields: [frontmatter___date], order: DESC }
        ) {
          edges {
            node {
              frontmatter {
                title
                date(formatString: "dddd [the] Do [of] MMMM YYYY")
                tags
                category
              }
              excerpt
              fields {
                slug
                draft
              }
            }
          }
        }
      }
      `)
      
      if (result.errors) {
        throw result.errors
      }
      
      createPaginatedPages({
        edges: result.data.allMarkdownRemark.edges,
        createPage: createPage,
        pageTemplate: postsTemplate,
        pathPrefix: 'posts',
        pageLength: postsPerPage,
      })
      
      const tagSet = new Set();
      
      const edges = result.data.allMarkdownRemark.edges
      
      edges.forEach(edge => {
        if (get(edge, 'node.frontmatter.tags') instanceof Array) {
          for (const tag of edge.node.frontmatter.tags) tagSet.add(kebabCase(tag));
        }
        createPage({
          path: edge.node.fields.slug,
          component: postTemplate,
          context: {
            slug: edge.node.fields.slug,
            draft: edge.node.fields.draft,
          }
        })
      })
      
      const tagList = Array.from(tagSet);
      await Promise.all(tagList.map(tag => {
        return graphql(`
          {
            allMarkdownRemark (
              limit: 1000
              sort: { fields: [frontmatter___date], order: DESC }
              filter: { fields: { tags: { in: ["${tag}"] } } }
            ) {
              edges {
                node {
                  frontmatter {
                    title
                    date(formatString: "dddd [the] Do [of] MMMM YYYY")
                    tags
                    category
                  }
                  excerpt
                  fields {
                    slug
                    draft
                  }
                }
              }
            }
          }
          `).then( results => {
            createPaginatedPages({
              edges: results.data.allMarkdownRemark.edges,
              createPage: createPage,
              pageTemplate: tagTemplate,
              pathPrefix: `tag/${tag}`,
              pageLength: postsPerPage,
              context: {
                tag: upperFirst(lowerCase(tag)),
              }
            })
          })
        })
      )
    }
    const createPages = async () => {
      const pageTemplate = path.resolve("src/templates/page.jsx")
      const result = await graphql(`
          {
            allMarkdownRemark(
              filter: { fields: { articleType: { eq: "page" } } }
            ) {
              edges {
                node {
                  frontmatter {
                    tags
                  }
                  fields {
                    slug
                  }
                }
              }
            }
          }
          `)
          
          if (result.errors) {
            throw result.errors
          }
          
          result.data.allMarkdownRemark.edges.forEach(edge => {
            createPage({
              path: edge.node.fields.slug,
              component: pageTemplate,
              context: {
                slug: edge.node.fields.slug
              }
            });
          });
    }
  
  try {
    var resultAll = await Promise.all([createPosts(), createPages()])
  } catch (error) {
    console.log('YOLO:', error)
  }
  return resultAll
}

"use strict";

var visit = require("unist-util-visit");
var isRelativeUrl = require("is-relative-url");
var fsExtra = require("fs-extra");
var path = require("path");
const fs = require('fs');
var _ = require("lodash");
var $ = require("cheerio");

module.exports = function (_ref) {
  var files = _ref.files,
      markdownNode = _ref.markdownNode,
      markdownAST = _ref.markdownAST,
      getNode = _ref.getNode;

  const filesToCopy = new Map()

  // Copy linked files to the public directory and modify the AST to point to
  // new location of the files.
  var visitor = function visitor(link) {
    if (isRelativeUrl(link.url) && getNode(markdownNode.parent).internal.type === "File") {
      var linkPath = path.join(getNode(markdownNode.parent).dir, link.url);
      var linkNode = _.find(files, function (file) {
        if (file && file.absolutePath) {
          return file.absolutePath === linkPath;
        }
        return null;
      });
      if (
        linkNode && linkNode.absolutePath &&
        _.has(markdownNode, 'fields.slug')
      ) {
        //fsExtra.mkdirs(path.join(process.cwd(),_.get(markdownNode, 'fields.slug')))
        var pathEnd = _.get(markdownNode, 'fields.slug') + '/' + linkNode.base;
        var newPath = path.join(process.cwd(), 'public', pathEnd);
        var relativePath = path.join("/" + pathEnd);
        let toStat;
        const fromStat = fs.lstatSync(linkPath);
        const toExists = fsExtra.existsSync(newPath);
        if (toExists) {
          toStat = fs.lstatSync(newPath);
        }
        link.url = "" + relativePath;
        if (!toExists || fromStat.mtime > toStat.mtime) {
          console.log(linkPath, '=>', newPath)
          fsExtra.copy(linkPath, newPath, function (err) {
            if (err) {
              console.error("error copying file", err);
            }
          });
        }
      }
    }
  };

  visit(markdownAST, "definition", function (link) {
    visitor(link);
  })

  visit(markdownAST, "link", function (link) {
    visitor(link);
  });

  // Also copy gifs since Sharp can't process them as well as svgs since we
  // exclude them from the image processing pipeline in
  // gatsby-remark-images. This will only work for markdown img tags
  visit(markdownAST, "image", function (image) {
    var imagePath = path.join(getNode(markdownNode.parent).dir, image.url);
    var imageNode = _.find(files, function (file) {
      if (file && file.absolutePath) {
        return file.absolutePath === imagePath;
      }
      return false;
    });
    if (imageNode && (imageNode.extension === "gif" || imageNode.extension === "svg")) {
      visitor(image);
    }
  });

  // Same as the above except it only works for html img tags
  visit(markdownAST, "html", function (node) {
    if (node.value.startsWith("<img")) {
      var image = Object.assign(node, $.parseHTML(node.value)[0].attribs);
      image.url = image.src;
      image.type = "image";
      image.position = node.position;

      var imagePath = path.join(getNode(markdownNode.parent).dir, image.url);
      var imageNode = _.find(files, function (file) {
        if (file && file.absolutePath) {
          return file.absolutePath === imagePath;
        }
        return false;
      });
      if (imageNode && (imageNode.extension === "gif" || imageNode.extension === "svg")) {
        visitor(image);
      }
    }
  });
  
  return Promise.all(
    Array.from(filesToCopy, async ([inPath, outPath]) => {
      console.log(linkPath, '=>', newPath)
    })
  )
};
